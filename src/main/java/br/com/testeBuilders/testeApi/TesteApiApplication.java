package br.com.testeBuilders.testeApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteApiApplication.class, args);
	}

}
