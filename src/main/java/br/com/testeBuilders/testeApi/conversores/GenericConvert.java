package br.com.testeBuilders.testeApi.conversores;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.testeBuilders.testeApi.dto.response.ClienteResponse;
import br.com.testeBuilders.testeApi.dto.response.RestResponsePage;

import java.lang.reflect.Type;
import java.util.List;

public class GenericConvert {

	public static <E, T> E convertModelMapper(T source, Class<E> typeDestination, MatchingStrategy strategy) {
        E model = null;
        if (source != null && typeDestination != null) {

             ModelMapper modelMapper = new ModelMapper();

             modelMapper.getConfiguration().setMatchingStrategy(strategy);
             model = modelMapper.map(source, typeDestination);
        }

        return model;
   }

   public static <E, T> E convertModelMapper(T source, Class<E> typeDestination) {
        return convertModelMapper(source, typeDestination, MatchingStrategies.STRICT);
   }
   
   public static <E, T> E convertWithMapping(T source, Class<E> typeDestination) {
       return convertModelMapper(source, typeDestination, MatchingStrategies.STRICT);
  }
   
   public static <E, T> E convertModelMapper(T source, Type destinationType) {

        E model = null;
        if (source != null && destinationType != null) {

             ModelMapper modelMapper = new ModelMapper();

             modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
             model = modelMapper.map(source, destinationType);
        }

        return model;
   }

   public static <E, T> void convertModelMapper(T source, E destination) {

        if (source != null && destination != null) {

             ModelMapper modelMapper = new ModelMapper();

             modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
             modelMapper.map(source, destination);
        }
   }
   public static <E, T> void convertModelMapper(T source, E destination, PropertyMap<T, E> mapping) {

        if (source != null && destination != null) {

             ModelMapper modelMapper = new ModelMapper();
             modelMapper.addMappings(mapping);
             modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
             modelMapper.map(source, destination);
        }
   }
   
   public static <E, T> List<E> convertModelMapper(List<T> source, Type destinationType) {
        return convertModelMapper(source, destinationType, MatchingStrategies.STRICT);
   }

   public static <E, T> List<E> convertModelMapper(List<T> source, Type destinationType, MatchingStrategy strategy) {

        List<E> model = null;
        if (source != null && destinationType != null) {

             ModelMapper modelMapper = new ModelMapper();

             modelMapper.getConfiguration().setMatchingStrategy(strategy);
             model = modelMapper.map(source, destinationType);
        }

        return model;
   }
   
   public static <E, T> List<E> convertWithMapping(List<T> source, Type destinationType, PropertyMap<T, E> mapping,MatchingStrategy strategy) {

        List<E> model = null;
        if (source != null && destinationType != null) {

             ModelMapper modelMapper = new ModelMapper();

             modelMapper.getConfiguration().setMatchingStrategy(strategy);
             modelMapper.addMappings(mapping);
             model = modelMapper.map(source, destinationType);
        }

        return model;
   }

   public static <E, T> List<E> convertWithMapping(List<T> source, Type destinationType, PropertyMap<T, E> mapping) {
        return convertWithMapping(source, destinationType, mapping, MatchingStrategies.STRICT);
   }

   public static <T, E> void convertWithMapping(T source, E destination, PropertyMap<T, E> mapping) {

        if (source != null && destination != null) {
             
             ModelMapper modelMapper = new ModelMapper();
             
             modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
             modelMapper.addMappings(mapping);
             modelMapper.map(source, destination);
        }

   }
   
   public static <T, E> E convertWithMapping(T source, Class<E> typeDestination, PropertyMap<T, E> mapping) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.addMappings(mapping);

        return modelMapper.map(source, typeDestination);
   }
   

   /**
    * Método para converter classes por meio de um mapa de propriedades, desconsiderando atributos nulos.
    *
    * @param source
    *      Objeto do qual deseja-se converter os dados.
    * @param typeDestination
    *      Tipo do objeto destino da converão.
    * @param mapping
    *      Map de propriedades a serem convertidas.
    * @return Um objeto do tipo E.
    */
   public static <T, E> E convertMappingNotNullProperties(T source, Class<E> typeDestination, PropertyMap<T, E> mapping) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Configurar para ignorar atributos nulos
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        // Verificar se foi passado um mapa de propriedades
        if (mapping != null) {
             // Adicionar map de propriedades
             modelMapper.addMappings(mapping);
        }
        return modelMapper.map(source, typeDestination);
   }

   /**
    * Método para converter classes por meio de um mapa de propriedades, desconsiderando atributos nulos.
    *
    * @param source
    *      Objeto do qual deseja-se converter os dados.
    * @param destination
    *      Objeto destino da converão.
    * @param mapping
    *      Map de propriedades a serem convertidas.
    */
   public static <T, E> void convertMappingNotNullProperties(T source, E destination, PropertyMap<T, E> mapping) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Configurar para ignorar atributos nulos
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        // Verificar se foi passado um mapa de propriedades
        if (mapping != null) {
             // Adicionar map de propriedades
             modelMapper.addMappings(mapping);
        }
        modelMapper.map(source, destination);
   }

   /**
    * Método para converter classes por meio de um mapa de propriedades, desconsiderando atributos nulos.
    *
    * @param source
    *      Objeto do qual deseja-se converter os dados.
    * @param destination
    *      Objeto destino da converão.
    */
   public static <T, E> void convertMappingNotNullProperties(T source, E destination) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Configurar para ignorar atributos nulos
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        // Verificar se foi passado um mapa de propriedades
        modelMapper.map(source, destination);
   }

}
