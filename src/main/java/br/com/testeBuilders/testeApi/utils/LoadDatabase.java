package br.com.testeBuilders.testeApi.utils;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.testeBuilders.testeApi.entity.Cliente;
import br.com.testeBuilders.testeApi.repository.ClienteRepository;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase {
	 
	@Bean
	  CommandLineRunner initDatabase(ClienteRepository repository) {
	    return args -> {
	      
	      log.info("Preloading " + repository.save(new Cliente("Karina da Silva", "97026308866", LocalDate.of(2000, 8, 12))));
	      log.info("Preloading " + repository.save(new Cliente("Maria Aparecida Lopes", "02357822899", LocalDate.of(1975, 2, 18))));
	      log.info("Preloading " + repository.save(new Cliente("Livia Borges Mangolin", "41899590811", LocalDate.of(1984, 2, 1))));
	      log.info("Preloading " + repository.save(new Cliente("Leonardo Augusto Mangolin", "58619736876", LocalDate.of(1995, 1, 4))));
	      log.info("Preloading " + repository.save(new Cliente("Ana Cristina Borges", "29895345801", LocalDate.of(1956, 11, 9))));
	      log.info("Preloading " + repository.save(new Cliente("Ivonete Borges de Lima", "35290707859", LocalDate.of(1997, 9, 15))));
	      log.info("Preloading " + repository.save(new Cliente("Andre Henrrique Redondo", "69659462883", LocalDate.of(1986, 8, 22))));
	      log.info("Preloading " + repository.save(new Cliente("Isabel da Cunha Moreira", "20081202873", LocalDate.of(1972, 5, 26))));
	      log.info("Preloading " + repository.save(new Cliente("Rebeca Andrade Betina", "50750961660", LocalDate.of(1957, 6, 13))));
	      log.info("Preloading " + repository.save(new Cliente("Joel Mangolin", "48571513678", LocalDate.of(1968, 10, 19))));
	      log.info("Preloading " + repository.save(new Cliente("Luciane Cristina Mangolin", "96748321492", LocalDate.of(1979, 12, 20))));
	      log.info("Preloading " + repository.save(new Cliente("Leandro Prestelo", "91685311415", LocalDate.of(2001, 1, 30))));
	      log.info("Preloading " + repository.save(new Cliente("Sandra Regina Rodrigues de Paula", "17959770190", LocalDate.of(1999, 3, 27))));
	      log.info("Preloading " + repository.save(new Cliente("Roberto Malaquias", "68043862133", LocalDate.of(1993, 6, 11))));
	      log.info("Preloading " + repository.save(new Cliente("Josue Rodrigues de Lima", "97571310911", LocalDate.of(2003, 10, 8))));
	      log.info("Preloading " + repository.save(new Cliente("Natanael Aparecido da Silva", "31692739816", LocalDate.of(1954, 9, 5))));
	      log.info("Preloading " + repository.save(new Cliente("Olivia Pires Nascimento", "75067545178", LocalDate.of(2000, 3, 31))));
	      log.info("Preloading " + repository.save(new Cliente("Marcos Madureira da Fonte", "49722157124", LocalDate.of(1992, 8, 2))));
	      
	    };
	  }
}
