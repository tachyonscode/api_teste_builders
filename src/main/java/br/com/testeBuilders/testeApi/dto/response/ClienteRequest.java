package br.com.testeBuilders.testeApi.dto.response;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ClienteRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	public ClienteRequest() {}
	
	@NotEmpty(message =  "Nome não pode ser vazio")
	private String nome;
	
	@CPF
	@NotEmpty(message =  "CPF não pode ser vazio")
	private String cpf;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@NotNull(message =  "Data nascimento não pode ser nula")
	private LocalDate dataNascimento;
	
}
