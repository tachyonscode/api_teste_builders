package br.com.testeBuilders.testeApi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.testeBuilders.testeApi.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
}

