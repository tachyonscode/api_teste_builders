package br.com.testeBuilders.testeApi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.testeBuilders.testeApi.entity.Cliente;

public interface ClienteService {

	Page<Cliente> listar(Cliente cliente, Pageable pageable);

	Cliente salvar(Cliente novoCliente);

	Cliente buscar(Long id);

	Cliente atualizar(Cliente novoCliente);
	
	Cliente atualizarParcial(Cliente cliente);

	void deletar(Long id);

}
