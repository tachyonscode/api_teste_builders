package br.com.testeBuilders.testeApi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.testeBuilders.testeApi.conversores.GenericConvert;
import br.com.testeBuilders.testeApi.entity.Cliente;
import br.com.testeBuilders.testeApi.exception.ClienteNotFoundException;
import br.com.testeBuilders.testeApi.repository.ClienteRepository;
import br.com.testeBuilders.testeApi.service.ClienteService;
import lombok.Data;

@Data
@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private final ClienteRepository repository;

	@Override
	public Page<Cliente> listar(Cliente cliente, Pageable pageable) {
		ExampleMatcher matcher =
				ExampleMatcher.matching()
				.withIgnoreCase()
				.withStringMatcher(StringMatcher.CONTAINING);

		Example<Cliente> exemplo = Example.of(cliente, matcher);

		return repository.findAll(exemplo, pageable);
	}

	@Override
	public Cliente salvar(Cliente novoCliente) {
		return repository.save(novoCliente);
	}

	@Override
	public Cliente buscar(Long id) {
		return repository.findById(id).orElseThrow(() -> new ClienteNotFoundException(id));
	}

	@Override
	public Cliente atualizar(Cliente cliente) {
		return repository.findById(cliente.getId())
				.map(c -> {
					c.setNome(cliente.getNome());
					c.setCpf(cliente.getCpf());
					c.setDataNascimento(cliente.getDataNascimento());
					return repository.save(c);
				})
				.orElseThrow(() -> new ClienteNotFoundException(cliente.getId()));

	}

	public Cliente atualizarParcial(Cliente cliente) {
		Cliente clienteDb = repository.findById(cliente.getId()).orElseThrow(() -> new ClienteNotFoundException(cliente.getId()));
		GenericConvert.convertMappingNotNullProperties(cliente, clienteDb, null);
		return repository.save(clienteDb);

	}

	@Override
	public void deletar(Long id) {
		Cliente cliente = repository.findById(id).orElseThrow(() -> new ClienteNotFoundException(id));
		repository.deleteById(cliente.getId());

	}

}
