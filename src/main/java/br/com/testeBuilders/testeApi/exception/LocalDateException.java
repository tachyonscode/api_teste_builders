package br.com.testeBuilders.testeApi.exception;

import java.time.LocalDate;

public class LocalDateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LocalDateException(LocalDate data) {
	    super("Formato LocalDate  " + data.toString());
	  }
	
}
