package br.com.testeBuilders.testeApi.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ObjectError {

	private final String message;
	private final String field;
	private final Object parameter;

}
