package br.com.testeBuilders.testeApi.exception;

public class ClienteNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClienteNotFoundException(Long id) {
	    super("Cliente não encontrado " + id);
	  }
	
}
