package br.com.testeBuilders.testeApi.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.testeBuilders.testeApi.conversores.GenericConvert;
import br.com.testeBuilders.testeApi.dto.response.ClientePatchRequest;
import br.com.testeBuilders.testeApi.dto.response.ClienteRequest;
import br.com.testeBuilders.testeApi.dto.response.ClienteResponse;
import br.com.testeBuilders.testeApi.dto.response.RestResponsePage;
import br.com.testeBuilders.testeApi.entity.Cliente;
import br.com.testeBuilders.testeApi.service.ClienteService;
import lombok.Data;

@Data
@RestController
@RequestMapping(value="/api/clientes")
public class ClienteResource {

	@Autowired
	private ClienteService servico;
	@Autowired
	MessageSource messageSource;

	@GetMapping
	ResponseEntity<?> listar(@RequestParam(name = "nome", defaultValue = "") String nome, 
			@RequestParam(name = "cpf", defaultValue = "") String cpf, Pageable pageable) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		Cliente cliente = new Cliente(nome, cpf);
		Page<Cliente> clientes = servico.listar(cliente, pageable);
		
		RestResponsePage<ClienteResponse> clienteResponse = 
				objectMapper.convertValue(clientes , new TypeReference<RestResponsePage<ClienteResponse>>() {});

		return ResponseEntity.ok(clienteResponse);
		
	}

	@PostMapping
	ResponseEntity<?> salvar(@RequestBody @Valid ClienteRequest clienteRequest) {
		
		Cliente cliente =  GenericConvert.convertWithMapping(clienteRequest, Cliente.class);
		return ResponseEntity.ok(servico.salvar(cliente));
	}

	@GetMapping("/{id}")
	ResponseEntity<?> buscar(@PathVariable Long id) {
		ClienteResponse clienteResponse =  GenericConvert.convertWithMapping(servico.buscar(id) , ClienteResponse.class);
		return ResponseEntity.ok(clienteResponse);
	}

	@PutMapping("/{id}")
	ResponseEntity<?> atualizar(@PathVariable Long id, @RequestBody @Valid ClienteRequest clienteRequest) {
		Cliente cliente =  GenericConvert.convertWithMapping(clienteRequest, Cliente.class);
		cliente.setId(id);
		ClienteResponse clienteResponse=  GenericConvert.convertWithMapping(servico.atualizar(cliente) , ClienteResponse.class);
		return ResponseEntity.ok(clienteResponse);
	}

	@DeleteMapping("/{id}")
	void deletar(@PathVariable Long id) {
		servico.deletar(id);
	}

	@PatchMapping("/{id}")
	ResponseEntity<?> atualizarParcial(@PathVariable Long id, @Valid @RequestBody ClientePatchRequest clienteRequest) {
		Cliente cliente = GenericConvert.convertWithMapping(clienteRequest, Cliente.class);
		cliente.setId(id);
		ClienteResponse clienteResponse =  GenericConvert.convertWithMapping(servico.atualizarParcial(cliente) , ClienteResponse.class);
		return ResponseEntity.ok(clienteResponse);
	}
}
