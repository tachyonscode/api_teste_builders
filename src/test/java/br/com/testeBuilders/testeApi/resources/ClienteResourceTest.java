package br.com.testeBuilders.testeApi.resources;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import br.com.testeBuilders.testeApi.entity.Cliente;
import br.com.testeBuilders.testeApi.repository.ClienteRepository;

//@WebMvcTest(ClienteResource.class)
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ClienteResourceTest {
	
	private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private static MockMvc mockMvc;

    @MockBean
    private ClienteRepository mockRepository;

    @Before
    public void init() {
    	Cliente cliente = new Cliente(1L, "Leonardo Mangolin", "31692739816", LocalDate.of(1984, 3, 26));
        when(mockRepository.findById(1L)).thenReturn(Optional.of(cliente));
    }
    
    @Test
    public void find_ClienteId_OK() throws Exception {

        mockMvc.perform(get("/api/clientes/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
		        .andExpect(jsonPath("$.name", is("Leonardo Mangolin")))
		        .andExpect(jsonPath("$.cpf", is("31692739816")))
		        .andExpect(jsonPath("$.dataNascimento", is(LocalDate.of(1984, 3, 26))));

        verify(mockRepository, times(1)).findById(1L);
        
    }

    @Test
    public void find_allClientes_OK() throws Exception {

        List<Cliente> clientes = Arrays.asList(
                new Cliente(1L, "Leonardo Augusto Mangolin", "31692739816", LocalDate.of(1984, 3, 26)),
                new Cliente(2L, "Livia Borges Mangolin", "30038728850", LocalDate.of(2014, 9, 2)));

        when(mockRepository.findAll()).thenReturn(clientes);

        mockMvc.perform(get("/clientes"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nome", is("Leonardo Augusto Mangolin")))
                .andExpect(jsonPath("$[0].cpf", is("31692739816")))
                .andExpect(jsonPath("$[0].dataNascimento", is(LocalDate.of(1984, 3, 26))))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].nome", is("Livia Borges Mangolin")))
                .andExpect(jsonPath("$[1].cpf", is("30038728850")))
                .andExpect(jsonPath("$[1].dataNascimento", is(LocalDate.of(2014, 9, 2))));

        verify(mockRepository, times(1)).findAll();
    }
    
    @Test
    public void find_ClienteIdNotFound_404() throws Exception {
        mockMvc.perform(get("/clientes/5")).andExpect(status().isNotFound());
    }
    
    @Test
    public void save_Cliente_OK() throws Exception {
    	Cliente cliente = new Cliente(1L, "Andre Henrrique Redondo", "31692739816", LocalDate.of(1995, 1, 15));
        when(mockRepository.save(any(Cliente.class))).thenReturn(cliente);

        mockMvc.perform(post("/clientes")
                .content(om.writeValueAsString(cliente))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nome", is("Andre Henrrique Redondo")))
                .andExpect(jsonPath("$.cpf", is("31692739816")))
                .andExpect(jsonPath("$.dataNascimento", is(LocalDate.of(1995, 1, 15))));

        verify(mockRepository, times(1)).save(any(Cliente.class));

    }
    
    @Test
    public void patch_ClienteCpf_OK() throws Exception {

        when(mockRepository.save(any(Cliente.class))).thenReturn(new Cliente());
        String patchInJson = "{\"cpf\":\"31692739816\"}";

        mockMvc.perform(patch("/clientes/1")
                .content(patchInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        verify(mockRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).save(any(Cliente.class));

    }
    
    @Test
    public void patch_ClienteCpf_405() throws Exception {

        String patchInJson = "{\"cpf\":\"31692739816\"}";

        mockMvc.perform(patch("/clientes/1")
                .content(patchInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());

        verify(mockRepository, times(1)).findById(1L);
        verify(mockRepository, times(0)).save(any(Cliente.class));
    }

    @Test
    public void delete_Cliente_OK() throws Exception {

        doNothing().when(mockRepository).deleteById(1L);

        mockMvc.perform(delete("/clientes/1"))
                .andExpect(status().isOk());

        verify(mockRepository, times(1)).deleteById(1L);
    }

}
